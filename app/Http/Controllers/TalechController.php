<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use kamermans\OAuth2\GrantType\ClientCredentials;
use kamermans\OAuth2\OAuth2Middleware;
use kamermans\OAuth2\OAuth2Subscriber;
use GuzzleHttp\HandlerStack;


class TalechController extends Controller
{
    private $clientId;
    private $clientSecret;
    private $refreshToken;
    private $grantType;
    private $host;
    private $auth;
    

    public function __construct(){
        $this->clientId = "Bmcghee.dev@gmail.com";
        $this->clientSecret = "WbAWuDDteyXKNnly";
        $this->refreshToken = "427324371/mEHPEddzkctQzwBhgNvjRIkD";
        $this->grantType = "refresh_token";
        $this->host = "https://mapi.talech.com/";
        $this->auth = "oauth";

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function get($path){
        $securityToken = $this->getAccessToken();

        $request = $this->getRequest($path);
    }
    public function getAccessToken(){
        $reauth_client = new Client([
            // URL for access_token request
            'base_uri' => $this->host
        ]);
        $reauth_config = [
            "client_id" => $this->clientId,
            "client_secret" => $this->clientSecret,
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'grant_type' => $this->grantType,        
            'refresh_token' => $this->refreshToken,
            "state" => time(), // optional
        ];
        $grant_type = new ClientCredentials($reauth_client, $reauth_config);
        $oauth = new OAuth2Middleware($grant_type);

        $stack = HandlerStack::create();
        $stack->push($oauth);

        $path = "o/oauth2/token";
        $request = $this->getRequest($path);

        $client = new Client([
            'auth' => $this->auth,
            'handler' => $stack,
        ]);
        $response = $client->send($request, ['http_errors' => true]);

        //echo json_encode($request->getHeaders());
        return $response->getBody();
        die;
        //var_dump($response->getBody);
        // var_dump($response);
        // die;
        return $response->getBody();
        die;
        if($response->getStatusCode() ===  200){
            return json_decode($response->getBody());
        } else if ($response->getStatusCode() === 404){
            return "404";
        } else {
            return $response;
        }

    }
    public function getRequest($path, $securityToken = null){
        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'grant_type' => $this->grantType,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
            'refresh_token' => $this->refreshToken
        ];

        $url = $this->host . $path;

        $request = new GuzzleRequest("POST", $url, $headers);

        if($securityToken != null){
            $request = modify_request($request, array('set_headers' => ['securityToken' => $securityToken]));
        }

        return $request;

    }
}
